import math
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
from gazebo_msgs.srv import SpawnModel
import rospy
from geometry_msgs.msg import Pose, Point, Quaternion

def spawnX():
  spawn_model_client = rospy.ServiceProxy('/gazebo/spawn_sdf_model', SpawnModel)
  spawn_model_client(
    model_name='BlueXPiece',
    model_xml=open('/home/tpc14/tictactoe/custom_pieces/BlueXPiece/model.sdf', 'r').read(), 
    robot_namespace='/moveit_commander',
    initial_pose=Pose(position= Point(0.2,0.2,0),orientation=Quaternion(0,0,0,0)),
    reference_frame='world'
  )
  #  /usr/share/gazebo-9/models/ground_plane/model.sdf'


def main():
  try:
    #tutorial.move_to_piece_factory()
    # tutorial.make_move(5)
    spawnX()

    print("============ Python tutorial demo complete!")
  except rospy.ROSInterruptException:
    return
  except KeyboardInterrupt:
    return

if __name__ == '__main__':
  main()
