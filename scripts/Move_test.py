import math
def getMovements(move):
    movements = [0,0]
    movements[0] = transformFunc(move%3)
    movements[1] = math.floor((move-1)/3)
    print(move,movements,(move-1)/3)

def transformFunc(num):
    if num == 0:
        return 1
    else:
        return num-2

for i in range(1,10):
    getMovements(i)