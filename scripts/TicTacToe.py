import random
import copy
import time
from Cartesian import MoveGroupPythonIntefaceTutorial,spawnBoard


class TicTacToeAI():
    def __init__(self):
        xpieces = ['X1','X2','X3','X4','X5']
        opieces = ['O1','O2','O3','O4','O5']
        board = ['Board']
        self.tutorial = MoveGroupPythonIntefaceTutorial()
        self.tutorial.removePieces(xpieces+opieces+board)
        time.sleep(1)
        spawnBoard(board[0])
        self.theBoard = [""] * 10
        self.playerLetter, self.computerLetter = self.inputPlayerLeter()
        self.turn = self.whoGoesFirst()
        print("The " + self.turn + " will go first.")
        self.gameIsPlaying = True
        self.startGameLoop()

    def drawBoard(self, board):
        print(" {:s} | {:s} | {:s} ".format(board[7], board[8], board[9]))
        print(" --------- ")
        print(" {:s} | {:s} | {:s} ".format(board[4], board[5], board[6]))
        print(" --------- ")
        print(" {:s} | {:s} | {:s} ".format(board[1], board[2], board[3]))

    def inputPlayerLeter(self):
        # Lets the player type which letter they want to be.
        # Returns a list with the player’s letter as the first item, and the computer's letter as the second.
        letter = ""
        while not (letter == "X" or letter == "O"):
            print("Do you want to be X or O")
            letter = input().upper()

        if letter == "X":
            return ("X", "O")
        else:
            return ("O", "X")

    def whoGoesFirst(self):
        if random.randint(0, 1) == 0:
            return "AI"
        else:
            return "Human"

    def playAgain(self):
        print("Do you want to play again? (yes or no)")
        return input().lower().startswith("y")

    def makeMove(self, board, letter, move):
        board[move] = letter

    def isWinner(self, board, letter):
        return ((board[7] == letter and board[8] == letter and board[9] == letter) or  # across the top
                # across the middle
                (board[4] == letter and board[5] == letter and board[6] == letter) or
                # across the bottom
                (board[1] == letter and board[2] == letter and board[3] == letter) or
                # down the left side
                (board[7] == letter and board[4] == letter and board[1] == letter) or
                # down the middle
                (board[8] == letter and board[5] == letter and board[2] == letter) or
                # down the right side
                (board[9] == letter and board[6] == letter and board[3] == letter) or
                # diagonal
                (board[7] == letter and board[5] == letter and board[3] == letter) or
                # diagonal
                (board[9] == letter and board[5] == letter and board[1] == letter))

    def getBoardCopy(self, board):
        return copy.deepcopy(board)

    def isSpaceFree(self, board, move):
        return board[move] == ""

    def getPlayerMove(self, board):
        move = ""
        while move not in "1 2 3 4 5 6 7 8 9".split() or not self.isSpaceFree(board, int(move)):
            print("What is your next move? (1-9)")
            move = input()
        return int(move)

    def chooseRandomMoveFromList(self, board, movesList):
        possibleMoves = []
        for i in movesList:
            if self.isSpaceFree(board, i):
                possibleMoves.append(i)

        if len(possibleMoves) != 0:
            return random.choice(possibleMoves)
        else:
            return None

    def getComputerMove(self, board, computerLetter):
        if computerLetter == "X":
            playerLetter = "O"
        else:
            playerLetter = "X"

        # Here is our algorithm for our Tic Tac Toe AI:
        # First, check if we can win in the next move
        for i in range(1, 10):
            copy = self.getBoardCopy(board)
            if self.isSpaceFree(copy, i):
                self.makeMove(copy, computerLetter, i)
                if self.isWinner(copy, computerLetter):
                    return i

        # Check if the player could win on their next move, and block them.
        for i in range(1, 10):
            copy = self.getBoardCopy(board)
            if self.isSpaceFree(copy, i):
                self.makeMove(copy, playerLetter, i)
                if self.isWinner(copy, playerLetter):
                    return i

        # Try to take one of the corners, if they are free.
        move = self.chooseRandomMoveFromList(board, [1, 3, 7, 9])
        if move != None:
            return move

        # Try to take the center, if it is free.
        if self.isSpaceFree(board, 5):
            return 5

        # Move on one of the sides.
        return self.chooseRandomMoveFromList(board, [2, 4, 6, 8])

    def isBoardFull(self, board):
        # Return True if every space on the board has been taken. Otherwise return False.
        for i in range(1, 10):
            if self.isSpaceFree(board, i):
                return False
        return True

    def startGameLoop(self):
        while self.gameIsPlaying:
            if self.turn == "Human":
                # Human's turn
                self.drawBoard(self.theBoard)
                move = self.getPlayerMove(self.theBoard)
                self.makeMove(self.theBoard, self.playerLetter, move)
                self.tutorial.make_move(move, self.playerLetter)

                if self.isWinner(self.theBoard, self.playerLetter):
                    self.drawBoard(self.theBoard)
                    print("You have won the game!")
                    self.gameIsPlaying = False
                else:
                    if self.isBoardFull(self.theBoard):
                        self.drawBoard(self.theBoard)
                        print("The game is a tie!")
                        break
                    else:
                        self.turn = "AI"
            else:
                # AI's Turn
                move = self.getComputerMove(self.theBoard, self.computerLetter)
                self.makeMove(self.theBoard, self.computerLetter, move)
                self.tutorial.make_move(move, self.computerLetter)

                if self.isWinner(self.theBoard, self.computerLetter):
                    self.drawBoard(self.theBoard)
                    print("The computer has won. You lose!")
                    self.gameIsPlaying = False
                else:
                    if self.isBoardFull(self.theBoard):
                        self.drawBoard(self.theBoard)
                        print("The game is a tie!")
                        break
                    else:
                        self.turn = "Human"


class TicTacToe():
    def __init__(self):
        xpieces = ['X1','X2','X3','X4','X5']
        opieces = ['O1','O2','O3','O4','O5']
        board = ['Board']
        self.tutorial = MoveGroupPythonIntefaceTutorial()
        self.tutorial.removePieces(xpieces+opieces+board)
        time.sleep(1)
        spawnBoard(board[0])
        self.theBoard = [""] * 10
        self.player1Letter, self.player2Letter = self.inputPlayer1Leter()
        self.turn = self.whoGoesFirst()
        print(self.turn + " will go first.")
        self.gameIsPlaying = True
        self.startGameLoop()

    def drawBoard(self, board):
        print(" {:s} | {:s} | {:s} ".format(board[7], board[8], board[9]))
        print(" --------- ")
        print(" {:s} | {:s} | {:s} ".format(board[4], board[5], board[6]))
        print(" --------- ")
        print(" {:s} | {:s} | {:s} ".format(board[1], board[2], board[3]))

    def inputPlayer1Leter(self):
        # Lets the player type which letter they want to be.
        # Returns a list with the player’s letter as the first item, and the computer's letter as the second.
        letter = ""
        while not (letter == "X" or letter == "O"):
            print("Will Player 1 be X or O")
            letter = input().upper()

        if letter == "X":
            return ("X", "O")
        else:
            return ("O", "X")

    def whoGoesFirst(self):
        if random.randint(0, 1) == 0:
            return "Player2"
        else:
            return "Player1"

    def playAgain(self):
        print("Do you want to play again? (yes or no)")
        return input().lower().startswith("y")

    def makeMove(self, board, letter, move):
        board[move] = letter

    def isWinner(self, board, letter):
        return ((board[7] == letter and board[8] == letter and board[9] == letter) or  # across the top
                # across the middle
                (board[4] == letter and board[5] == letter and board[6] == letter) or
                # across the bottom
                (board[1] == letter and board[2] == letter and board[3] == letter) or
                # down the left side
                (board[7] == letter and board[4] == letter and board[1] == letter) or
                # down the middle
                (board[8] == letter and board[5] == letter and board[2] == letter) or
                # down the right side
                (board[9] == letter and board[6] == letter and board[3] == letter) or
                # diagonal
                (board[7] == letter and board[5] == letter and board[3] == letter) or
                # diagonal
                (board[9] == letter and board[5] == letter and board[1] == letter))

    def isSpaceFree(self, board, move):
        return board[move] == ""

    def getPlayerMove(self, board):
        move = ""
        while move not in "1 2 3 4 5 6 7 8 9".split() or not self.isSpaceFree(board, int(move)):
            print("What is " + self.turn + "'s next move? (1-9)")
            move = input()
        return int(move)

    def isBoardFull(self, board):
        # Return True if every space on the board has been taken. Otherwise return False.
        for i in range(1, 10):
            if self.isSpaceFree(board, i):
                return False
        return True

    def startGameLoop(self):
        while self.gameIsPlaying:
            if self.turn == "Player1":
                # Player 1's turn
                self.drawBoard(self.theBoard)
                move = self.getPlayerMove(self.theBoard)
                self.makeMove(self.theBoard, self.player1Letter, move)
                self.tutorial.make_move(move, self.player1Letter)

                if self.isWinner(self.theBoard, self.player1Letter):
                    self.drawBoard(self.theBoard)
                    print("Player 1 has won the game!")
                    self.gameIsPlaying = False
                else:
                    if self.isBoardFull(self.theBoard):
                        self.drawBoard(self.theBoard)
                        print("The game is a tie!")
                        break
                    else:
                        self.turn = "Player2"
            else:
                # Player 2's Turn
                self.drawBoard(self.theBoard)
                move = self.getPlayerMove(self.theBoard)
                self.makeMove(self.theBoard, self.player2Letter, move)
                self.tutorial.make_move(move, self.player2Letter)

                if self.isWinner(self.theBoard, self.player2Letter):
                    self.drawBoard(self.theBoard)
                    print("Player 2 has won the game!")
                    self.gameIsPlaying = False
                else:
                    if self.isBoardFull(self.theBoard):
                        self.drawBoard(self.theBoard)
                        print("The game is a tie!")
                        break
                    else:
                        self.turn = "Player1"


playGame = True

while playGame:
    print("Do you want to play against the AI? (yes or no)")
    if input().lower().startswith("y"):
        game = TicTacToeAI()
        playGame = game.playAgain()
    else:
        game = TicTacToe()
        playGame = game.playAgain()